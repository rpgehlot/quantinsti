
module.exports = function(db){
	var instrumentModel = {};
	instrumentModel.getAll = function(callback){
		var query = "SELECT instrumentid, position FROM Instruments";
		db.executeSelectQuery(query,function(err,rows){
				if(err)
					callback(err);
				else callback(null,rows);
		});
	};

	instrumentModel.getById = function(instrumentId,callback){
		var query = "SELECT instrumentid,position FROM Instruments WHERE instrumentid="+instrumentId;
		db.executeSelectQuery(query,function(err,rows){
				if(err)
					callback(err);
				else callback(null,rows);
		});
	};

	instrumentModel.update = function(data,callback){
			var position,arr,query = "SELECT position FROM Instruments WHERE instrumentid = " + data.id;
			db.executeSelectQuery(query,function(err,rows){
					if(err)
						callback(err);
					else{
						
						if(rows.length===0){
							position = 0;
							query = "INSERT INTO Instruments(instrumentid,position) VALUES(?,?)";
							if(data['side'] == 2)
								position -= parseInt(data['size']);
							else position += parseInt(data['size']);

							arr = [data.id,position.toString()];
							db.executeInsertQuery(query,arr,function(err){
									if(err)
										callback(err);
									else
										callback();										
							});
						}
						else{
							position = parseInt(rows[0]['position']);
							if(data.side == 2)
								position -= parseInt(data['size']);
							else position += parseInt(data['size']);
							query = "UPDATE Instruments SET position=" + position +" WHERE instrumentid="+data.id;
							db.executeUpdateQuery(query,function(err,result){
									if(err)
										callback(err);
									else callback();
							});
						}
		
					}
			});
	};
	return instrumentModel;
};