var express = require('express');
var router = express.Router();

/* GET home page. */
module.exports = function(AppController){
	router.get('/', AppController.index);
	router.get('/instrument/:id',AppController.showInstrumentPage);
	router.get('/showAll',AppController.showAll);
	return router;
};

