var express = require('express');
var router = express.Router();

/* GET users listing. */
module.exports = function(AppController){
	router.get('/getstatus', AppController.getstatus);
	router.get('/getstatusById/:id',AppController.getstatusById);
	return router;
}

