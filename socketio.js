module.exports = function(socketio,InstrumentModel){
	socketio.on('connect',function(){
	    console.log('connection established with the cron job server at port : 8080');
	});

	socketio.on('instrumentData',function(data){
			if(data==null)
				return;
			var tmp = data.split(':')[1],xx={};
			if(tmp!=null && tmp.split('/').length){
				tmp.split('|').forEach(function(entry){
						var cf = entry.split('=');
						if(cf[0]==32)
							xx['size'] = cf[1];
						if(cf[0]==54)
							xx['side'] = cf[1];
						if(cf[0]==48)
							xx['id'] = cf[1];
				});	
				if(xx.hasOwnProperty('size') && xx.hasOwnProperty('side') && xx.hasOwnProperty('id')){
			    	InstrumentModel.update(xx,function(err){
			    			if(err)
			    				console.log(err);
			    			else console.log('value updated in db');
			    	});
			    }
		    }
	});
	return true;
};