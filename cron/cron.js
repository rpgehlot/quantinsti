var express = require('express');
var app = express();
var multer = require('multer');
var server = require('http').Server(app);
var socketio = require('socket.io')(server);
var readline = require('readline');


var port = 8080;
var upload = multer();

app.use('/plugins',express.static(__dirname+'/../node_modules'));
app.use('/assets',express.static(__dirname+'/../public'));
app.get('/',function(req,res){
		res.sendFile(__dirname+'/index.html');
});

socketio.on('connection',function(socket){
	console.log('connection Received');
	socket.on('instrumentUpdateRequest',function(data){
			socket.broadcast.emit('instrumentData',data);
	});
});

server.listen(port,function(req,res){
	console.log('Cron Job Server runnning at port : ' + port);
});
