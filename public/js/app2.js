(function(angular,socketio){
	'use strict';
	var MainApp = angular.module('MainApp',['ngMaterial']);

	MainApp.service('backendservice',['$http','$timeout',function($http,$timeout){
			this.emitEvent = function(data){
					socketio.emit('instrumentUpdateRequest',data);
					return;
			};		
		

			this.getInstrumentStatus = function(callback){
					$http.get('/api/getstatus').then(
						function(data){
								console.log(data);
								callback(data.data);			
						},
						function(){

						}
					);
			};	

			this.getposition = function(id,callback){
					$http.get('/api/getstatusById/'+id).then(
						function(data){
								console.log(data);
								$timeout(function(){
									callback(data.data);
								},500);
						},
						function(){

						}
					);
			};


	}]);



	MainApp.controller('GetController',['$scope','backendservice',function($scope,backendservice){
					$scope.instruments = [];
					$scope.data = {
						position : null,
						instrumentid : null,
						isloading : false,
						isresult : true
					};
					backendservice.getInstrumentStatus(function(status){
							for(var i=0 ;i< status.length ;i++){
								if(status[i].position>0)
									status[i].isup = true;
								else status[i].isup = false;
							}
							$scope.instruments = status;
					});

					$scope.getposition = function(){
							$scope.data.isloading = true;
							backendservice.getposition($scope.data.instrumentid,function(data){
									$scope.data.isloading = false;
									if(data.hasOwnProperty('position')){
										$scope.data.position = data.position;
										$scope.data.isresult = true;
									}
									else {
										$scope.data.position = null;
										$scope.data.isresult = false;
									}
							});
					};
	}]);



	MainApp.controller('InstrumentController',['$scope','backendservice',function($scope,backendservice){
				$scope.instrument = instrument;
	}]);



	MainApp.controller('ShowAllController',['$scope','backendservice','$interval',function($scope,backendservice,$interval){
					$scope.instruments = [];
					backendservice.getInstrumentStatus(function(status){
							for(var i=0 ;i< status.length ;i++){
								if(status[i].position>0)
									status[i].isup = true;
								else status[i].isup = false;
							}
							$scope.instruments = status;
					});
					var interval = $interval(function(){
							backendservice.getInstrumentStatus(function(status){
									for(var i=0 ;i< status.length ;i++){
										if(status[i].position>0)
											status[i].isup = true;
										else status[i].isup = false;
									}
									$scope.instruments = status;
							});
					},5000);
	}]);



	MainApp.controller('CronController',['$scope','backendservice','$timeout',function($scope,backendservice,$timeout){
			$scope.instrumentdata = null;
			$scope.instruments = [];
			$scope.isFileModeEnabled = false;
			$scope.file=null;
			$scope.SendInstrumentOrder = function(){					
					backendservice.emitEvent($scope.instrumentdata);
					$scope.instruments.push($scope.instrumentdata);
					$scope.instrumentdata = null;
			};

			$scope.checkinstrumentdata = function(){
					return $scope.instrumentdata == null || $scope.instrumentdata == '';
			};

			$scope.UploadFile = function(){
					
					var CHUNK_SIZE = 2000;
					var FILE_SIZE = $scope.file.size - 1;
					var arr = [],file;

					file = $scope.file;
					$scope.file=null;
					
					function emitevent(i){
						if(i>arr.length)
							return;
						$scope.instruments.push(arr[i]);
						backendservice.emitEvent(arr[i]);
						$timeout(function(){
							emitevent(i+1);
						},900);
					}

					function parse(start){
						if(start > FILE_SIZE){
							emitevent(0);
							return;
						}
						var reader = new FileReader();
			            var blob = file.slice(start, CHUNK_SIZE + start);
			            reader.onload = function(){
			            		// console.log(reader.result);
			            		var content = reader.result;
			            		content.split('\n').forEach(function(line){
			            			arr.push(line);
			            		});
			            		parse(start+CHUNK_SIZE);
			            };
			            reader.readAsText(blob);						
					}
					parse(0);

			};

			socketio.on('instrumentData',function(data){
					$scope.$apply(function(){
						$scope.instruments.push(data);
					});					
			});
	}]);

	// directive to read file
	MainApp.directive('fileread',function(){
			return{
				restrict : 'A',
				link : function(scope,element,attrs){
						element.on('change',function(event){							
							scope.$apply(function(){
								scope.file = event.target.files[0];
							});								
						});
				}
			};
	});


	MainApp.config(function($mdThemingProvider){
			$mdThemingProvider.theme('default')
			.primaryPalette('green')
			.accentPalette('red');

	});


})(angular,socket);