module.exports = function(InstrumentModel){
	var controller = {};

	controller.index = function(req,res,next){
		res.render('index');		
	};

	controller.showAll = function(req,res,next){
		res.render('showAll');
	};

	controller.showInstrumentPage = function(req,res,next){
		var InstrumentId = req.params.id;
		console.log(InstrumentId);
		InstrumentModel.getById(InstrumentId, function(err,rows){
				if(err){
					res.render('error',{
						msg : 'InstrumentId : ' + InstrumentId + ' does not exist in db.'
					});
				}
				else{
					if(rows.length===0){
						res.render('error',{msg : 'InstrumentId : ' + InstrumentId + ' does not exist in db.'});
					}
					else{
						console.log( JSON.stringify(rows[0]));
						res.render('instrument',{instrument : JSON.stringify(rows[0])});
					}
				}
		});
	};

	controller.getstatus = function(req,res){
			InstrumentModel.getAll(function(err,rows){
					if(err)
						res.json(err);
					else res.json(rows);
			});
	};

	controller.getstatusById = function(req,res){
			var InstrumentId = req.params.id;
			InstrumentModel.getById(InstrumentId, function(err,rows){
				if(err){
					res.end(err);
				}
				else{
					if(rows.length)
						res.json(rows[0]);
					else res.json({});
				}
			});
	};
	return controller;
};