var mysql = require('mysql');

//constructor function for database object
var _cachedDBInstances = {};
function DB(dbConfig){
	if(!(this instanceof DB))
			return new DB(dbConfig);
		
	this.dbConfig = dbConfig;
	this.createPool();
}

DB.prototype.createPool = function(){
	this.pool = mysql.createPool({
				connectionLimit : 100, //important
				host  : this.dbConfig.DB_HOST,
				user  : this.dbConfig.DB_USERNAME,
				password  : this.dbConfig.DB_PASSWORD,
				database : this.dbConfig.DB_DATABASE
	});
	return;
};

DB.prototype.executeSelectQuery = function(query,callback){

	this.pool.getConnection(function(err,connection){
			if(err)
				callback(err);
			else{
				connection.query(query,function(err,rows){
						if(err)
							callback(err);
						else callback(null,rows);
						
						connection.release();
				});
			}
	});
	
};


DB.prototype.executeInsertQuery = function(query,arr,callback){

	this.pool.getConnection(function(err,connection){
			if(err){
					console.error('Error fetching a connnection to save the customer');
					callback(err);
			}
			else{
					console.log('Successfully fetched connection from pool with connection id = ' + connection.threadId);
					
					connection.query(query,arr,function(err,result){
							callback(err,result);
							connection.release();
					});
			}
	});
};

DB.prototype.executeUpdateQuery = function(query,callback){
	
	this.pool.getConnection(function(err,connection){
			if(err){
					console.error('Error fetching a connnection to save the customer');
					callback(err);
			}
			else{
					console.log('Successfully fetched connection from pool with connection id = ' + connection.threadId);
					
					connection.query(query,function(err,result){
							callback(err,result);
							connection.release();
					});
			}
	});
};

// use of dependency injection for loose coupling and higher cohesion resulting in higher maintainability and reusalability.
module.exports = function(dbConfig){
		
			// method based on dependency injection

		var 
			// database instance object variable
			db,
			// getting the database configuration object
			// fetching the database id from the config
			_databaseId = dbConfig.DB_ID;

		// chekcing if the database is already instantiated if not ..then we do it.	
		if(!_cachedDBInstances.hasOwnProperty(_databaseId)){

			console.log('New database instance is created');
			db = new DB(dbConfig);
			_cachedDBInstances[_databaseId] = db;
		
		}
		
		return _cachedDBInstances[_databaseId];
		
};
