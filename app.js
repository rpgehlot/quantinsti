var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var socketio = require('socket.io-client')('http://localhost:8080');


var diContainer = require('./dicontainer')();
diContainer.register('socketio',socketio);
diContainer.register('dbConfig',require('./config'));
diContainer.factory('db',require('./db'));
diContainer.factory('InstrumentModel',require('./models/InstrumentModel.js'));
diContainer.factory('AppController',require('./controllers/AppController.js'));
diContainer.factory('route',require('./routes/index.js'));
diContainer.factory('apiroutes',require('./routes/apiroutes.js'));
diContainer.factory('SocketFactory',require('./socketio'));
var routes = diContainer.get('route');
var apiroutes = diContainer.get('apiroutes');
diContainer.get('SocketFactory');

var app = express();
var port = process.env.port || 3000;
app.listen(port);
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/plugins',express.static(path.join(__dirname,'node_modules')));
app.use('/assets',express.static(path.join(__dirname,'public')));
app.use('/', routes);
app.use('/api',apiroutes);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    msg : '404 NOT FOUND',
    message: err.message,
    error: {}
  });
});


module.exports = app;
