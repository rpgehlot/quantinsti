DROP DATABASE IF EXISTS instrumentdata;
CREATE DATABASE instrumentdata;

use instrumentdata;

CREATE TABLE Instruments(	
	uuid int(11) unsigned NOT NULL AUTO_INCREMENT,
	instrumentid varchar(100) NOT NULL,
	position varchar(100) NOT NULL,
	PRIMARY KEY (uuid),
	UNIQUE(instrumentid)
)ENGINE=INNODB  DEFAULT CHARSET=utf8;
