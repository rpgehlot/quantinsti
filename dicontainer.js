var arglist = require('args-list');

module.exports = function(){

	var factories    =  {};
	var dependencies =  {};
	var dicontainer  =  {};

	// method to register a factory function.
	dicontainer.factory = function(name,factory){
			factories[name] = factory;
	};

	// method to register a stateful instance of dependency.
	dicontainer.register = function(name,instance){
			dependencies[name] = instance;
	};

	// method to get the stateful instance of a dependency. 
	dicontainer.get = function(name){
			if(!dependencies[name]){
				var factory = factories[name];
				dependencies[name] = factory && dicontainer.inject(factory);
				if(!dependencies[name])
					throw new Error('Cant find the module : '+ name);
			}
			return dependencies[name];
	};

	// invokes the factory function with required resolved dependencies to get stateful instance.
	dicontainer.inject = function(factory){
			var args = arglist(factory).map(function(dependency){
					return dicontainer.get(dependency);
			});
			return factory.apply(null,args);
	};

	return dicontainer;
};
