# Steps to run the application. #
*  npm install
*  mysql < db.sql to create a database table 
    if there is no user 
    else enter corresponding user and also update config.js file with the    required credentials.
*  To run the main website enter 'node app.js'
*  To run the cron job app 
      -navigate to cron folder.
      -node cron.js
* you can view website on localhost:3000 or whatever your process.env.port is set to.
*  cron app will run specifically on port 8080; 
     - otherwise make the change in cron.js file
*  cron app : localhost:8080

* website api endpoints : 
  * / 
*   /instrument/:id
*   /showAll

* cron job endpoint:
1.  /
you can upload the filllist file there or can enter a single entry.
both options are provided.